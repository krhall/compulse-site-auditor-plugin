<?php

/**
 * @var $this CompulseSiteAuditorPlugin
 */

?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.4.4/css/scroller.dataTables.min.css" />

<style>
    div.auditor-table-container {
        padding: 10px 20px 10px 5px;
        font-family: Arial;
        font-size: 14px;
    }

    table.auditor-table {
        width: 100%;
        border-collapse: collapse;
        z-index: 100000;
    }

    table.auditor-table tr:hover td {
        background-color: #bbbbbb;
    }

    table.auditor-table th, table.auditor-table td {
        padding: 3px;
        text-align: center;
    }

    table.auditor-table thead.fixed {
        position: fixed;
    }

    table.auditor-table thead tr th {
        vertical-align: middle;
        background-color: #f1f1f1;
    }

    table.auditor-table tbody tr:first-child td {
        border-top: 2px solid #000000;
    }

    table.auditor-table tbody tr td {
        border-bottom: 2px solid #eeeeee;
        background-color: #dddddd;
        transition: background-color .25s;
    }

    table.auditor-table tbody tr.odd td {
        background-color: #cccccc;
    }

    table.auditor-table tbody tr td.ignored {
        opacity: .25;
    }

    table.auditor-table tbody tr.odd:hover td {
        background-color: #bbbbbb;
    }

    @keyframes update-flash-even {
        0% {
            background-color: #888888;
        }

        100% {
            background-color: #dddddd;
        }
    }

    @keyframes update-flash-odd {
        0% {
            background-color: #888888;
        }

        100% {
            background-color: #cccccc;
        }
    }

    table.auditor-table tbody tr.update-flash td {
        animation: update-flash forwards 1s;
    }

    table.auditor-table tbody tr.odd.update-flash td {
        animation: update-flash-odd forwards 1s;
    }

    table.auditor-table tbody tr.passed td {
        background-color: #aaffaa;
    }

    table.auditor-table tbody tr.info td {
        background-color: #ddffdd;
    }

    table.auditor-table tbody tr.warning td {
        background-color: #ffffaa;
    }

    table.auditor-table tbody tr.error td {
        background-color: #ffaaaa;
    }

    table.auditor-table td.passed {
        background-color: #aaffaa !important;
    }

    table.auditor-table td.info {
        background-color: #ddffdd !important;
    }

    table.auditor-table td.warning {
        background-color: #ffff00 !important;
    }

    table.auditor-table td.error {
        background-color: #ffaaaa !important;
    }

    table.auditor-table td.blank {
        background-color: inherit !important;
    }

    table.auditor-table tr td a:focus {
        box-shadow: none !important;
        outline: 0 !important;
    }

    table.auditor-table i.fa.status-icon {
        font-size: 20px;
    }

    tr.show-more-info a.view-more-info i.fa:before {
        content: "\f0d8";
    }

    tr.show-more-info + tr.more-info {
        display: table-row;
    }

    tr.more-info {
        display: none;
    }

    tr.more-info td {
        text-align: left;
        background-color: #cccccc !important;
    }

    .tippy-tooltip {
        font-size: 14px !important;
    }

    .auditor-table-container .floatThead-container {
        box-shadow: -5px 0px 10px 0px #000
    }

    .dataTables_wrapper .dataTables_filter {
        width: 40%;
        margin-bottom: 10px;
    }

    .dataTables_wrapper .dataTables_filter label {
        width: 100%;
    }

    .dataTables_wrapper .dataTables_filter input {
        padding: 5px;
        width: 80%;
    }

    .dataTables_wrapper .dataTables_length select {
        padding: 5px;
    }
</style>

<div class="auditor-table-container">
    <table class="auditor-table">
        <thead>
            <tr>
                <th>Install Name</th>
                <th>Server</th>
                <th>Domain</th>
                <th>Site Title</th>
                <th>Last Audit</th>
                <th>Overall</th>
                <th>Disk</th>
                <th>Enabled</th>
                <th>Launched</th>
                <th>Domain</th>
                <th>Search</th>
                <th>Analytics</th>
                <th>WP Core</th>
                <th>SSL</th>
                <th>Misc</th>
                <!-- <th>&nbsp;</th> --> <!-- More Info -->
                <th>&nbsp;</th> <!-- Run Audit Now -->
            </tr>
        </thead>

        <tbody>
            <?php

            $results = get_posts([
                'post_type' => 'auditor_result',
                'post_status' => 'all',
                'posts_per_page' => -1,
                'orderby' => 'meta_value',
                'order' => 'ASC',
                'meta_key' => 'install_name',
                'fields' => 'ids'
            ]);

            $results_by_install_name = [];

            foreach ( $results as $result_id ) {
                $install_name = get_post_meta($result_id, 'install_name', true);
                $results_by_install_name[ $install_name ] = $result_id;
            }

            // foreach ( $results as $result_id ) {
            //     print $this->get_auditor_table_row( $result_id );
            // }

            $all_sites = $this->get_site_list_array();
            sort($all_sites);

            foreach ( $all_sites as $install_name ) {
                if ( array_key_exists($install_name, $results_by_install_name) ) {
                    print $this->get_auditor_table_row( $results_by_install_name[ $install_name ] );
                } else { ?>
                    <tr data-install-name="<?php print $install_name; ?>">
                        <td><?php print $install_name; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Never</td>
                        <?php for ($i = 1; $i <= 10; $i++): ?>
                            <td></td>
                        <?php endfor; ?>
                        <td><a href="#" title="Run Audit Now" class="run-audit-now"><i class="fa fa-refresh"></i></a></td>
                    </tr>
                <?php }
            }

            ?>
        </tbody>
    </table>
</div>

<script>
$ = jQuery;
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.16/api/row().show().js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://unpkg.com/tippy.js@2.5.2/dist/tippy.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>

<?php

$config = [
    'search' => isset($_GET['search']) ? $_GET['search'] : '',
    'plugin_name' => isset($_GET['plugin_name']) ? $_GET['plugin_name'] : '',
    'url' => get_the_permalink()
];

?>

<script>

    var config = <?php echo json_encode( $config ); ?>;

    (function($) {
        var auditorDataTable;

        $(function() {
            var statusDataFunc = function(row, type, val, meta, index) {
                var priorityKey = 'priority' + index;
                var displayKey = 'display' + index;

                if ( type == 'set' ) {
                    var priority = 999;

                    if ( val ) {
                        var priorityValue = parseInt( $(val).data('priority') );
                        if ( priorityValue ) {
                            priority = priorityValue;
                        }
                    }

                    row[priorityKey] = priority;
                    row[displayKey] = val ? val : ' ';

                    //console.log('set');

                    return row[priorityKey];
                } else if ( type == 'filter' ) {
                    return row[priorityKey];
                } else if ( type == 'sort' ) {
                    return row[priorityKey];
                }

                //console.log(type);

                return row[displayKey] ? row[displayKey] : '0';
            };

            auditorDataTable = $('table.auditor-table').DataTable({
                pageLength: 50,
                columns: [
                    null, // 0 Install Name
                    null, // 1 Server
                    null, // 2 Domain
                    null, // 3 Site Title
                    null, // 4 Last Audit
                    { // 5 Overall Status
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 5);
                        }
                    },
                    { // 6 Disk
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 6);
                        }
                    },
                    { // 7 Enabled
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 7);
                        }
                    },
                    { // 8 Launched
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 8);
                        }
                    },
                    { // 9 Domain
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 9);
                        }
                    },
                    { // 10 Search
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 10);
                        }
                    },
                    { // 11 Analytics
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 11);
                        }
                    },
                    { // 12 WP Core
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 12);
                        }
                    },
                    { // 13 SSL
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 13);
                        }
                    },
                    { // 14 Misc
                        data: function(row, type, val, meta) {
                            return statusDataFunc(row, type, val, meta, 14);
                        }
                    },
                    { // 15 Audit Now
                        orderable: false
                    }
                ],
                order: [
                    [0, "asc"]
                ],
                fixedHeader: {
                    header: true,
                    headerOffset: $('body').hasClass('admin-bar') ? 32 : 0
                },
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
            });

            $.fn.dataTable.ext.search.push( function( settings, data, dataIndex ) {
                var pluginName = $('#plugin_name').val();

                if ( pluginName ) {
                    var node = auditorDataTable.row( dataIndex ).node();
                    if ( node ) {
                        var el = $( node );
                        var activePlugins = el.data( 'active-plugins' );

                        // console.log(activePlugins);

                        if ( activePlugins ) {
                            if ( activePlugins[ pluginName ] ) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            } );

            $('.dataTables_filter').append( '<label style="display:block;"><span class="plugin-name-help" title="Enter the plugin\'s folder name. For example, to find all sites using Advanced Custom Fields, enter \'advanced-custom-fields-pro\'. A list of plugins is below in the Statistics section."><i class="fa fa-question-circle"></i></span> Plugin Name: <input type="text" id="plugin_name" /></label>' );

            $('#plugin_name').keyup( function() {
                auditorDataTable.draw();
            } );

            var redraw = false;

            if ( config.plugin_name ) {
                $("#plugin_name").val( config.plugin_name );
                redraw = true;
            }

            if ( config.search ) {
                auditorDataTable.search( config.search );
                redraw = true;
            }

            if ( redraw ) {
                auditorDataTable.draw();
            }

            $('.dataTables_filter input[type="search"], #plugin_name').on( 'keyup', _.debounce(function() {
                history.replaceState({}, '', config.url + "?search=" + $('.dataTables_filter input[type="search"]').val() + "&plugin_name=" + $( '#plugin_name' ).val());
            }, 500) );
        });

        $("table.auditor-table").on('click', 'a.view-more-info', function(e) {
            var row = $(this).closest('tr');
            row.toggleClass('show-more-info');

            e.preventDefault();
            return false;
        });

        // $("table.auditor-table").floatThead({
        //     position: 'fixed',
        //     top: $('body').hasClass('admin-bar') ? 32 : 0
        // });

        $("table.auditor-table").on('click', 'a.run-audit-now', function(e) {
            var row = $(this).closest('tr');
            var installName = row.data('install-name');
            var icon = $(this).find('i.fa');

            icon.addClass('fa-spin');

            //console.log('audit site: ', installName);

            $.post('/wp-admin/admin-ajax.php', {
                'action': 'compulse_run_audit_now',
                'install_name': installName
            }, function(data) {
                //console.log(data);

                if (data.success) {
                    var newRow = $( data['row_html'] );
                    var newRowNode = newRow[0];

                    auditorDataTable.row( row ).remove();
                    auditorDataTable.row.add( newRowNode ).draw().show().draw(false);

                    var scrollTop = $( newRowNode ).position().top;
                    console.log(scrollTop);

                    $('html, body').animate({
                        'scrollTop': (scrollTop - 100) + 'px'
                    }, 'fast', 'swing', function() {
                        $( newRowNode ).addClass('update-flash');
                        setTimeout(function() {
                            $( newRowNode ).removeClass('update-flash');
                        }, 1000);
                    });
                }

                icon.removeClass('fa-spin');
            });

            e.preventDefault();
            return false;
        });

        tippy('table.auditor-table', {
            target: 'tbody tr td[title]'
        });

        tippy( '.plugin-name-help' );
    })(jQuery);
</script>
