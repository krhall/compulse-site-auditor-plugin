<?php

/**
 * Run command 'php audit-test.php' to run the auditor via command line.
 * Change $installs to the sites that you want to audit.
 */

exit;

include 'audit.php';

$tests_to_run = true;

$installs = [
    'krhall2'
];

foreach ($installs as $install) {
    $auditor = new SiteAuditor($install);

    if ( is_array($tests_to_run) ) {

    } elseif ( $tests_to_run === true ) {
        $auditor->run_all_tests();
    }

    var_dump($auditor->get_all_test_results());
}
