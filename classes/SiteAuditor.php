<?php

/**
 * An audit for a single site.
 * This object is passed to every SiteAuditTest and grows with more test results and metadata as those tests complete.
 * Once all tests are complete, this object can be used to retrieve audit information.
 */
class SiteAuditor {
    private $install_name;
    private $last_test_results;
    private $site_data;

    private $meta_data;

    private $ssh_connection;
    private $logger;

    public function __construct($install_name) {
        $this->install_name = $install_name;
        $this->last_test_results = [];
        $this->site_data = [];

        $this->meta_data = [];

        $this->ssh_connection = new SiteAuditorSSHConnection( $this );
        $this->logger = new SiteAuditorLogger();
    }

    public function add_meta_data( $name, $value ) {
        $this->meta_data[ $name ] = $value;
    }

    public function get_meta_data() {
        return $this->meta_data;
    }

    /**
     * @return SiteAuditorLogger
     */
    public function get_logger() {
        return $this->logger;
    }

    public function get_install_name() {
        return $this->install_name;
    }

    private function run_test(SiteAuditTest $test) {
        SiteAuditorUtils::set_logger( $this->logger );

        $this->logger->log( 'Starting test "' . $test->get_name() . '" for install ' . $this->get_install_name() );
        $this->logger->start_elapsed_time();

        $this->last_test_results[ $test->get_name() ] = $test->run( $this );

        $this->logger->log_elapsed_time( 'Finished test "' . $test->get_name() . '" for install ' . $this->get_install_name() . '.' );
        $this->logger->stop_elapsed_time();
        $this->logger->log( '--------------------' );
    }

    public function error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
        $this->logger->log( 'An error occurred: ' . $errstr . ' in ' . $errfile . ' on line ' . $errline . '.' );
    }

    public function run($test_name) {
        $test = SiteAuditTestManager::get_instance()->get_test($test);

        if ( !empty($test) ) {
            $this->run_test($test);
        }
    }

    public function has_test_result($test_name) {
        return ( array_key_exists($test_name, $this->last_test_results) && !empty( $this->last_test_results[ $test_name ] ) );
    }

    public function get_test_result($test_name) {
        return array_key_exists($test_name, $this->last_test_results) ? $this->last_test_results[ $test_name ] : null;
    }

    public function get_all_test_results() {
        return $this->last_test_results;
    }

    public function run_all_tests() {
        set_error_handler( array($this, 'error_handler') );

        $this->logger->log( 'Running all tests for install ' . $this->get_install_name() );
        $this->logger->log( '--------------------' );
        $this->logger->start_elapsed_time();

        $all_tests = SiteAuditTestManager::get_instance()->get_all_tests();

        foreach ($all_tests as $test) {
            $this->run_test($test);
        }

        $this->logger->log_elapsed_time( 'Finished running all tests for install ' . $this->get_install_name() . '.' );
        $this->add_meta_data( 'elapsed_time', $this->logger->get_elapsed_time() );
        $this->logger->stop_elapsed_time();
        $this->logger->log( '====================' );

        restore_error_handler();

        $this->ssh_connection->disconnect();
    }

    public function get_wp_engine_url($secure = false) {
        return 'http' . ($secure ? 's' : '') . '://' . $this->install_name . '.wpengine.com';
    }

    public function get_site_data($force_update = false, $use_url = null) {
        if ( $force_update || empty($this->site_data) ) {
            $result_code = 0;
            $count = 0;

            $url = $use_url ? $use_url : $this->get_wp_engine_url() . '/';

            do {
                $data = SiteAuditorUtils::get_url_data( $url );
                $this->site_data[] = $data;

                $result_code = $data['info']['http_code'];
                $count++;

                if ( array_key_exists('Location', $data['headers']) ) {
                    $url = $data['headers']['Location'];

                    if ( !preg_match('/^https?:\/\//', $url) ) {
                        $matches = [];
                        if ( preg_match('/^https?:\/\/[^\/]+/', $data['url'], $matches) ) {
                            $last_domain = $matches[0];

                            $url = $last_domain . '/' . ltrim($url, "/ \t\n\r\0\x0B");
                        }
                    }
                } else {
                    break;
                }
            } while ( in_array($result_code, [301, 302]) && $count < 10  );
        }

        return $this->site_data;
    }

    public function get_ssh_connection() {
        return $this->ssh_connection;
    }

    public function close_ssh_connection() {
        if ( !empty( $this->ssh_connection ) ) {
            $this->ssh_connection->disconnect();
        }
    }
}
