<?php

class DiskUsageTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('disk');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        // return $result;

        /*
         * These commands aren't working correctly.
         * Output keeps overlapping for some sites and commands don't run properly.
         * Multiple tests in a row cause the script to hang on the live site.
         */

        // Check disk usage via SSH connection.
        $ssh = $auditor->get_ssh_connection();

        $command_results = [
            'total' => $ssh->send_command( 'du -sk ~/sites/' . $auditor->get_install_name() ),
            'uploads' => $ssh->send_command( 'du -sk ~/sites/' . $auditor->get_install_name() . '/wp-content/uploads' ),
            //'database' => $ssh->send_command( 'mysql information_schema -N -B -e "SELECT ROUND(SUM(data_length + index_length) / 1024, 1) AS total_size FROM tables WHERE table_schema=\'wp_' . $auditor->get_install_name() . '\'"' )
        ];

        // Format results.

        array_walk( $command_results, function( &$result ) {
            if ( !empty( $result ) ) {
                $result = intval( explode( "\t", $result[0] )[0] );
            } else {
                $result = false;
            }
        } );

        $auditor->add_meta_data( 'disk', $command_results );

        if ( !empty( $command_results['total'] ) ) {
            $result->add_message( 'Site is using a total of ' . SiteAuditorUtils::format_kb( $command_results['total'] ) . ' of disk space.', 'passed' );
        } else {
            $result->add_message( 'Unable to determine total disk usage.', 'info' );
        }

        if ( !empty( $command_results['uploads'] ) ) {
            $result->add_message( 'Site is using ' . SiteAuditorUtils::format_kb( $command_results['uploads'] ) . ' of disk space for uploads.', 'passed' );
        }

        return $result;
    }
}
