<?php

class LaunchedTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('launched');
    }

    private function is_development_url( $url ) {
        return ( strpos($url, '.wpengine.com') !== FALSE || strpos($url, '.compulse-staging.com') !== FALSE );
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        $enabled = ($auditor->get_test_result('enabled')->get_status() == 'passed');

        if ( $enabled ) {
            // Check if site has launched.

            $site_data = $auditor->get_site_data();
            $num_site_data = count($site_data);
            $final_url = $site_data[ $num_site_data - 1 ]['url'];

            $has_launched = ( $num_site_data > 1 && !$this->is_development_url( $final_url ) );

            if ( !$has_launched ) {
                // If the final site URL is a development URL, check the feed to make sure the <link> is also a development URL.
                // If it's not, this means the site is actually launched, but there's no redirect.
                $auditor->get_logger()->log( 'Checking feed URL for a site URL.' );
                $feed_url = $auditor->get_wp_engine_url() . '/feed/';

                $feed_data = SiteAuditorUtils::get_url_data( $feed_url );
                $matches = [];
                if ( preg_match( '/<link>([^<]+)<\/link>/', $feed_data['body'], $matches ) ) {
                    $feed_site_url = $matches[1];
                    //var_dump($feed_site_url);

                    if ( !$this->is_development_url( $feed_site_url ) ) {
                        $auditor->get_logger()->log( 'Site is launched, but is not redirecting from development URL.' );
                        // Update the site data using the url from the feed.
                        $site_data = $auditor->get_site_data( true, $feed_site_url );
                        $num_site_data = count($site_data);
                        $final_url = $site_data[ $num_site_data - 1 ]['url'];
                        //var_dump($final_url);

                        $has_launched = ( $num_site_data > 1 && !$this->is_development_url( $final_url ) );
                    }
                }
            }

            if ( $has_launched ) {
                $result->add_message('Site has launched at ' . $site_data[ $num_site_data - 1 ]['url'], 'passed');
            } else {
                $result->add_message('Site has not launched, so some checks will be skipped.', 'info');
            }
        } else {
            $auditor->get_logger()->log( 'Skipping launched test because site is not enabled.' );
        }

        return $result;
    }
}
