<?php

/**
 * Handles logging information to a log file in the auditor directory.
 */
class SiteAuditorLogger {
    private $entries;
    private $start_times;
    private $log_file_name;

    public function __construct() {
        $this->entries = [];
        $this->start_times = [];
        $this->log_file_name = COMPULSE_AUDIT_DIR . '/audit-' . date( 'm-d-y' ) . '-' . md5( 'auditorlog::' . date( 'm-d-y' ) ) . '.log';
    }

    public function start_elapsed_time() {
        $this->start_times[] = microtime( true );
    }

    public function get_elapsed_time() {
        return microtime( true ) - $this->start_times[ count( $this->start_times ) - 1 ];
    }

    public function stop_elapsed_time() {
        return !empty( $this->start_times ) ? microtime( true ) - array_pop( $this->start_times ) : 0;
    }

    public function restart_elapsed_time() {
        $this->stop_elapsed_time();
        $this->start_elapsed_time();
    }

    public function log( $message ) {
        $entry = '[' . date( 'm/d/y H:i:s' ) . ']: ' . $message . "\n";
        $this->entries[] = $entry;
        error_log( $entry, 3, $this->log_file_name );
    }

    public function log_var( $var ) {
        $this->log( print_r( $var, true ) );
    }

    public function log_elapsed_time( $message, $restart = false ) {
        $elapsed_time = $this->get_elapsed_time();
        $this->log( $message . ' Elapsed Time: ' . $elapsed_time . ' seconds' );

        if ( $restart ) {
            $this->restart_elapsed_time();
        }
    }

    public function get_entries() {
        return $this->entries;
    }
}
