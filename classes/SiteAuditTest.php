<?php

/**
 * Parent class for all audit tests.
 * Running this test on its own just runs dependencies, and returns an new, empty test result.
 * Tests should extend this class and override run().
 */
abstract class SiteAuditTest {
    /**
     * The name of this test.
     * @var string
     */
    private $name;

    /**
     * The current audit being performed.
     * @var SiteAuditor
     */
    private $current_auditor;

    /**
     * Names of tests that must run before this test can run.
     * @var string[]
     */
    private $dependencies;

    public function __construct($name, $dependencies = []) {
        $this->name = $name;
        $this->current_auditor = null;
        $this->dependencies = $dependencies;
    }

    /**
     * Retrieves the current site being audited.
     * @return SiteAuditor
     */
    protected function get_current_auditor() {
        return $this->current_auditor;
    }

    /**
     * Retrieve the name of this test.
     * @return string
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * Run this test on the specified site and return the result.
     * @param SiteAuditor $auditor The current audit.
     * @return SiteAuditTestResult
     */
    public function run(SiteAuditor $auditor) {
        $this->current_auditor = $auditor;

        // Run dependencies first, if they haven't already ran.
        foreach ($this->dependencies as $dep) {
            if ( !$auditor->has_test_result($dep) ) {
                $auditor->run($dep);
            }
        }

        $result = new SiteAuditTestResult();

        return $result;
    }
}
