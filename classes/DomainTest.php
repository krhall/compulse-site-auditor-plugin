<?php

class DomainTest extends SiteAuditTest {
    private $cloudflare_api_key = '54da6918fd7b0e025222de1cff7d6d63b4537';

    public function __construct() {
        parent::__construct('domain');
    }

    private function are_compulse_cloudflare_ns_records( $records ) {
        if ( !empty( $records ) && count( $records ) == 2 ) {
            return (
                ($records[0]['target'] == 'austin.ns.cloudflare.com' && $records[1]['target'] == 'jean.ns.cloudflare.com') ||
                ($records[0]['target'] == 'jean.ns.cloudflare.com' && $records[1]['target'] == 'austin.ns.cloudflare.com')
            );
        }

        return false;
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        // Check that the domain on the install is pointing to the correct place.
        $site_data = $auditor->get_site_data();
        $num_site_data = count( $site_data );

        $enabled = ( $auditor->get_test_result('enabled')->get_status() == 'passed' );
        $launched = ( $auditor->get_test_result('launched')->get_status() == 'passed' );


        // First check if the enabled and launched checks passed.
        if ( $enabled && $launched ) {
            // Site is enabled and launched.
            // Get the IP address of the WP Engine install (this is the correct IP for the site).
            $wpengine_domain = $auditor->get_install_name() . '.wpengine.com';
            $wpengine_dns_record = dns_get_record( $wpengine_domain, DNS_A );

            if ( empty( $wpengine_dns_record ) ) {
                $result->add_message( 'Failed to retrieve site\'s IP address.', 'info' );
            } else {
                $correct_ip = $wpengine_dns_record[0]['ip'];
                $auditor->get_logger()->log( 'Site\'s correct IP address is: ' . $correct_ip );

                // Get the final URL of the site (after redirects).
                $final_site_data = $site_data[ $num_site_data - 1 ];
                $final_site_url = $final_site_data['url'];

                // Extract the base domain from the final URL of the site.
                $base_domain = SiteAuditorUtils::get_base_domain( $final_site_url );

                // Extract the base domain, including any subdomains.
                $base_domain_with_subdomain = SiteAuditorUtils::get_base_domain( $final_site_url, true );

                // Extract the subdomain portion of the site, including the trailing .
                $subdomain = str_replace($base_domain, '', $base_domain_with_subdomain);

                // true if the site's main URL is on a subdomain.  This is used in various checks below.
                $site_lives_on_subdomain = !in_array($subdomain, ['', 'www.']);

                $domains_to_check = [];

                if ( $site_lives_on_subdomain ) {
                    // Only check the subdomain
                    $domains_to_check = [$base_domain_with_subdomain];
                } else {
                    // Check the base domain and the base domain with "www." in front.
                    $domains_to_check = [$base_domain, 'www.' . $base_domain];
                }

                $auditor->get_logger()->log( 'Auditor will check these domains: ' . implode( ', ', $domains_to_check ) );

                // Get the nameservers of the base domain.
                $base_domain_ns_records = dns_get_record( $base_domain, DNS_NS );
                // var_dump($final_site_url, $base_domain, $base_domain_ns_records);


                if ( empty( $base_domain_ns_records ) ) {
                    $result->add_message( 'Failed to retrieve base domain NS records.', 'info' );
                } else {

                    if ( $this->are_compulse_cloudflare_ns_records( $base_domain_ns_records ) ) {
                        // Base domain is using our Cloudflare DNS.

                        // Check the records via the Cloudflare API.
                        try {
                            $cloudflare_records = SiteAuditorUtils::get_cloudflare_dns_records( $base_domain );

                            if ( !empty( $cloudflare_records ) ) {
                                $proxied_record = false;

                                foreach ( $domains_to_check as $domain_to_check ) {
                                    $found_record = false;

                                    // Find the domain to check within the retrieved Cloudflare records.
                                    foreach ( $cloudflare_records as $record ) {
                                        if ( $record['name'] == $domain_to_check && in_array( $record['type'], [ 'A', 'CNAME' ] ) ) {
                                            // Found an A or CNAME record for the domain.
                                            $auditor->get_logger()->log( 'Found Cloudflare DNS record. ' . $record['type'] . ': ' . $record['name'] . ' -> ' . $record['content'] );

                                            if ( $found_record ) {
                                                // If we've already found an A/CNAME record for the domain, that's a misconfiguration.
                                                $result->add_message( $record['name'] . ' has multiple A/CNAME records in Cloudflare.', 'warning' );
                                            }

                                            $found_record = true;

                                            if ( $record['proxied'] ) {
                                                // Domain is using Cloudflare caching.
                                                $proxied_record = true;
                                            }

                                            if ( $record['type'] == 'A' ) {
                                                // Record is an A record in Cloudflare.  All records in Cloudflare should be CNAME records.
                                                if ( $domain_to_check == $base_domain ) {
                                                    $replace_with = 'CNAME Flattening pointed to ' . $wpengine_domain;
                                                } else {
                                                    $replace_with = 'a CNAME record pointed to ' . ( $site_lives_on_subdomain ? $wpengine_domain : $base_domain );
                                                }

                                                if ( $record['content'] != $correct_ip ) {
                                                    // Domain is using an A record that is pointing to the wrong IP address.
                                                    $result->add_message( $record['name'] . ' is not pointing to the correct IP address.  It needs to be updated to use ' . $replace_with . '.', 'error' );
                                                } else {
                                                    // Domain is using a correct A record, but it should still be replaced with CNAME flattening or a CNAME record.
                                                    $result->add_message( $record['name'] . ' needs to be updated to use ' . $replace_with . '.', 'warning' );
                                                }
                                            } elseif ( $record['type'] == 'CNAME' ) {
                                                if ( $domain_to_check == $base_domain && $record['content'] != $wpengine_domain ) {
                                                    // Domain is the base domain and it's using CNAME flattening, but it's not pointing to the correct install.
                                                    $result->add_message( $record['name'] . ' is not pointing to the correct WP Engine domain.  It needs to be updated to point to ' . $wpengine_domain . '.', 'error' );
                                                } elseif ( $domain_to_check != $base_domain && !in_array( $record['content'], [ $wpengine_domain, $base_domain ] ) ) {
                                                    // Domain is a subdomain and it's using a CNAME record, but it's neither pointing to the WP Engine url nor the base domain.
                                                    $result->add_message( $record['name'] . ' is not pointing to the correct record.  It needs to be updated to point to ' . ( $site_lives_on_subdomain ? $wpengine_domain : $base_domain ) . '.', 'error' );
                                                } else {
                                                    $result->add_message( $record['name'] . ' is set up correctly in our Cloudflare account.' );
                                                }
                                            }
                                        }
                                    } // end foreach record
                                } // end foreach domain to check

                                if ( $proxied_record ) {
                                    $result->set_flag( 'cloudflare_caching' );
                                    $result->add_message( 'One or more records are using Cloudflare caching. Disable this if there are issues with the SSL certificate or if updates are not appearing.', 'info' );
                                }
                            } else {
                                $result->add_message( $base_domain . ' is using our Cloudflare nameservers, but no records were found in Cloudflare.', 'error' );
                            }
                        } catch ( Exception $e ) {
                            // Cloudflare API error.
                            $result->add_message( 'Failed to check DNS records due to an error: ' . $e->getMessage(), 'warning' );
                        }
                    } else {
                        // Site is not using our Cloudflare DNS.

                        // List of known nameservers and their names.
                        $known_dns = [
                            'bluehost.com' => 'BlueHost',
                            'bnin.net' => 'Brightnet',
                            'canaca.net' => 'Canaca',
                            'charter.net' => 'Spectrum Business',
                            'cloudflare.com' => 'Cloudflare',
                            'deluxehosting.com' => 'Deluxe Hosting',
                            'directnic.com' => 'Directnic.com',
                            'dnsbycomodo.net' => 'Comodo',
                            'dnsimple.com' => 'DNSimple',
                            'domaincontrol.com' => 'GoDaddy',
                            'domainpeople.com' => 'DomainPeople',
                            'dynect.net' => 'Dyn.com',
                            'easydns.com' => 'easyDNS',
                            'easydns.net' => 'easyDNS',
                            'easydns.org' => 'easyDNS',
                            'googledomains.com' => 'Google Domains',
                            'hansoninfosys.com' => 'Hanson Information Systems',
                            'hostgator.com' => 'HostGator',
                            'inmotionhosting.com' => 'InMotion Hosting',
                            'justhost.com' => 'JustHost',
                            'lunarpages.com' => 'Lunarpages',
                            // 'maildesk.net' => '???',
                            'mdnsservice.com' => 'Homestead',
                            'meganameservers.com' => 'MegaNameServers',
                            'name-services.com' => 'Enom',
                            'ns0.com' => 'pair Domains',
                            'ntsia.com' => 'Network Technology Solutions',
                            'oppenheimergroup.com' => 'One Sky Media',
                            'pair.com' => 'pair Domains',
                            'pairnic.com' => 'pair Domains',
                            'readyhosting.com' => 'ReadyHosting.com',
                            'realssl.com' => 'ResellerChoice',
                            'register.com' => 'Register.com',
                            'registrar-servers.com' => 'Namecheap',
                            'saratogahosting.net' => 'Saratoga Hosting',
                            'scarabmedia.com' => 'Scarab Media',
                            // 'siteground327.com' => 'SiteGround',  (uses many different domains)
                            'turtlehut.com' => 'Turtlehut',
                            'ui-dns.biz' => '1&1 Ionos',
                            'ui-dns.com' => '1&1 Ionos',
                            'ui-dns.de' => '1&1 Ionos',
                            'ui-dns.org' => '1&1 Ionos',
                            'whois.com' => 'Whois.com',
                            'wixdns.net' => 'Wix',
                            'worldnic.com' => 'Network Solutions',
                            'yahoo.com' => 'Yahoo! Small Business',
                            // 'yourhostingaccount.com' => '???',
                            'zoneedit.com' => 'Zoneedit',
                        ];

                        // The base domain of the nameservers.
                        $ns_domain = strtolower( SiteAuditorUtils::get_base_domain( $base_domain_ns_records[0]['target'] ) );

                        $all_ns = [];

                        foreach ( $base_domain_ns_records as $ns_record ) {
                            $all_ns[] = $ns_record['target'];
                        }

                        // Make a note that this domain isn't in our Cloudflare account.
                        if ( isset( $known_dns[$ns_domain] ) ) {
                            $result->add_message( $base_domain . ' is using nameservers through ' . $known_dns[$ns_domain] . ' (' . implode( ' / ', $all_ns ) . ')' );
                        } else {
                            $result->add_message( $base_domain . ' is using these nameservers: ' . implode( ' / ', $all_ns ) );
                        }

                        $wpengine_proxied_ips = [
                            '104.199.123.216' => [ '35.225.175.237' ],
                            '104.198.2.82' => [ '35.225.175.237' ],
                            '104.199.114.220' => [ '35.225.175.237' ],
                            '104.197.135.66' => [ '35.225.175.237' ],
                            '35.203.142.3' => [ '35.225.175.237' ],
                            '35.199.186.26' => [ '35.225.175.237' ],
                            '35.225.175.237' => [ // compulseweb separation
                                '35.184.226.240', // compulse1
                                '34.74.188.198', // compulse2
                                '35.196.250.249', // compulse3
                                '104.196.40.7', // compulse4
                                '35.229.54.236', // compulse5
                                '104.198.57.224', // compulse6
                                '35.224.126.81', // compulse7
                                '34.72.14.41', // compulse8
                            ]
                        ];

                        // Get the DNS records via PHP.
                        foreach ( $domains_to_check as $domain_to_check ) {
                            // Try to get a CNAME record for the domain first.
                            $domain_to_check_record = dns_get_record( $domain_to_check, DNS_CNAME );

                            if ( empty( $domain_to_check_record ) ) {
                                // No CNAME record found, try an A record.
                                $domain_to_check_record = dns_get_record( $domain_to_check, DNS_A );
                            }

                            if ( empty( $domain_to_check_record ) ) {
                                $result->add_message( 'No A or CNAME records found for ' . $domain_to_check, 'error' );
                            } else {
                                // Merge in defaults for A and CNAME records.
                                $record = array_merge( [
                                    'ip' => '',
                                    'target' => ''
                                ], $domain_to_check_record[0] );

                                $auditor->get_logger()->log( 'Found DNS record. ' . $record['type'] . ': ' . $record['host'] . ' -> ' . $record['ip'] . $record['target'] );

                                if ( $domain_to_check == $base_domain ) {
                                    // Checks for the base domain (will be an A record).

                                    // The record is not pointing to the correct IP address.
                                    if ( $record['ip'] != $correct_ip ) {
                                        // Check if it eventually gets to the correct IP address through redirects.
                                        if ( $final_site_data['info']['primary_ip'] == $correct_ip ) {
                                            $result->add_message( $domain_to_check . ' is pointing to the wrong IP address (' . $record['ip'] . '), but it is being redirected to a page (' . $final_site_data['url'] . ') with the correct IP address (' . $correct_ip . ').', 'info' );
                                        } else {
                                            // Check if it's being proxied.

                                            // Is it a Cloudflare IP?
                                            // 104.16.0.0/12
                                            // 104.16.0.0 through 104.31.255.255
                                            // 104       . 16
                                            // 0000 0000 . 0001 0000
                                            $cloudflare_ip_ranges = [
                                                [ ip2long('104.16.0.0'), ip2long('104.31.255.255') ],
                                                [ ip2long( '172.64.0.0' ), ip2long( '172.79.255.255' ) ]
                                            ];

                                            $record_ip_long = ip2long( $record['ip'] );

                                            $is_cloudflare_ip = false;

                                            foreach ( $cloudflare_ip_ranges as $range ) {
                                                if ( $range[0] <= $record_ip_long && $record_ip_long <= $range[1] ) {
                                                    $is_cloudflare_ip = true;
                                                    break;
                                                }
                                            }

                                            if ( $is_cloudflare_ip ) {
                                                $result->set_flag( 'cloudflare_caching' );
                                                $result->add_message( $domain_to_check . ' is being proxied through a Cloudflare account that does not belong to us (IP: ' . $record['ip'] . '). DNS records could not be verified.', 'info' );
                                                break; // Don't check any more records.
                                            } else {
                                                // Check for headers indicating another proxy.
                                                if ( isset( $final_site_data['headers']['X-Sucuri-ID'] ) ) {
                                                    // Sucuri proxy
                                                    $result->add_message( $domain_to_check . ' is being proxied through Sucuri (IP: ' . $record['ip'] . '). DNS records could not be verified.', 'info' );
                                                } else {
                                                    // Is it being proxied by WP Engine after a server migration?
                                                    if ( isset( $wpengine_proxied_ips[ $record['ip'] ] ) && in_array( $correct_ip, $wpengine_proxied_ips[ $record['ip'] ] ) ) {
                                                        $result->add_message( $domain_to_check . ' is pointing to an old IP address (' . $record['ip'] . ') and WP Engine is proxying it. It needs to be updated to point to ' . $correct_ip . '.', 'warning' );
                                                    } else {
                                                        // Wrong...
                                                        $result->add_message( $domain_to_check . ' is pointing to the wrong IP address (' . $record['ip'] . '). It needs to be updated to point to ' . $correct_ip . '.', 'error' );
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $result->add_message( $domain_to_check . ' is pointing to the correct IP address (' . $correct_ip . ').' );
                                    }
                                } else {
                                    // Checks for subdomains.
                                    if ( $record['type'] == 'A' && $record['ip'] != $correct_ip ) {
                                        if ( isset( $wpengine_proxied_ips[ $record['ip'] ] ) && in_array( $correct_ip, $wpengine_proxied_ips[ $record['ip'] ] ) ) {
                                            $result->add_message( $domain_to_check . ' is pointing to an old IP address (' . $record['ip'] . ') and WP Engine is proxying it. It needs to be updated to point to ' . $correct_ip . '.', 'warning' );
                                        } else {
                                            // Wrong...
                                            $result->add_message( $domain_to_check . ' is pointing to the wrong IP address (' . $record['ip'] . ').  It needs to be updated to point to ' . $correct_ip . '.', 'error' );
                                        }
                                    } elseif ( $record['type'] == 'CNAME' && !in_array( $record['target'], $site_lives_on_subdomain ? [$wpengine_domain] : [$wpengine_domain, $base_domain] ) ) {
                                        $result->add_message( $domain_to_check . ' is pointing to the wrong domain (' . $record['target'] . ').  It needs to be updated to point to ' . $wpengine_domain . '.', 'error' );
                                    } else {
                                        $result->add_message( $domain_to_check . ' is pointing to the correct ' . ($record['type'] == 'A' ? 'IP address (' . $correct_ip . ')' : 'domain (' . $record['target'] . ')') . '.' );
                                    }
                                } // End if this is the base domain.
                            } // End if DNS record for the domain was retrieved.

                        } // End foreach domain to check.
                    } // end if are our Cloudflare nameservers

                    // Check MX records, if everything else passed.
                    if ( in_array( $result->get_status(), ['passed','info'] ) && !$site_lives_on_subdomain ) {
                        $mx_records = dns_get_record( $base_domain, DNS_MX );
                        if ( !empty( $mx_record ) ) {
                            foreach ( $mx_records as $mx_record ) {
                                if ( $mx_record['target'] == $base_domain ) {
                                    $result->add_message( 'The MX record for ' . $base_domain . ' is pointing to the base domain. If email is being used on the domain, it will no longer work.', 'error' );
                                }
                            }
                        }
                    }

                    // Check SPF record at some point?
                } // end if base domain NS records were retrieved.
            } // end if retrieved WP Engine domain's IP address.
        } else { // end if enabled && launched
            $auditor->get_logger()->log( 'Skipping domain test because site is either not enabled or not launched.' );
        }


        return $result;
    }
}
