<?php

class MiscTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('miscellaneous');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        $enabled = ($auditor->get_test_result('enabled')->get_status() == 'passed');
        $launched = ($auditor->get_test_result('launched')->get_status() == 'passed');
        $domain_valid = ($auditor->get_test_result('domain')->get_status() != 'error');

        if ( $enabled ) {
            $issue_found = false;

            $site_data = $auditor->get_site_data();
            $last_site_data = $site_data[ count($site_data) - 1 ];

            if ( false ) {
                $typekit_regexp = '/(?:typekit|fonts\.adobe\.com)/';

                // Check for a typekit font on this page.
                if ( preg_match( $typekit_regexp, $last_site_data['body'] ) ) {
                    $result->add_message( 'Adobe Fonts is being used on the site.', 'warning' );
                    $issue_found = true;
                } else {
                    // Check for a typekit font in all local (/cache or /theme) stylesheets.
                    $stylesheet_matches = [];
                    if ( preg_match_all( '#href=["\']([^"\']+/wp\-content/(?:themes|cache)/[^"\']+\.css(?:\?[^"\']+)?)["\']#', $last_site_data['body'], $stylesheet_matches ) ) {
                        // var_dump( $stylesheet_matches );
                        foreach ( $stylesheet_matches[1] as $stylesheet ) {
                            // var_dump( $stylesheet );
                            $stylesheet_contents = file_get_contents( $stylesheet );

                            // var_dump( substr( $stylesheet_contents, 0, 200 ) );

                            if ( preg_match( $typekit_regexp, $stylesheet_contents ) ) {
                                $result->add_message( 'Adobe Fonts is being used on the site.', 'warning' );
                                $issue_found = true;
                                break;
                            }
                        }
                    }
                }
            }




            if ( $launched && $domain_valid ) {
                // Check for old Google Maps key somewhere in the code.
                $old_key = 'AIzaSyBEH76k9jufiTmurjPd31UWYqYGkXW5IzA';

                $matches = [];

                if ( preg_match('/' . $old_key . '/', $last_site_data['body'], $matches) ) {
                    $issue_found = true;
                    $result->add_message('Something on the site is using the shared Google Maps API key. The map should be updated to use an embed or to have its own key.', 'warning');
                }
            }




            if ( $launched && $domain_valid ) {
                // Check for node_modules folder
                // Check sitedomain.com/node_modules if it exists.

                // Check via SSH...
                $ssh = $auditor->get_ssh_connection();

                $command_result = $ssh->send_command( 'test -d ~/sites/' . $auditor->get_install_name() . '/node_modules && echo "1"' );

                if ( trim( $command_result[0] ?? '' ) == '1' ) {
                    $issue_found = true;
                    $result->add_message( 'There is a node_modules folder on the live site. This should be deleted.', 'warning' );
                }

                // Previous way of checking via HTTP...
                // $site_data = $auditor->get_site_data();
                // $last_site_data = $site_data[ count($site_data) - 1 ];
                // $domain = SiteAuditorUtils::get_base_domain( $last_site_data['url'], true );
                //
                // $node_modules_url = 'http://' . $domain . '/node_modules/';
                //
                // $all_node_modules_data = SiteAuditorUtils::get_url_data_with_redirects( $node_modules_url );
                // $node_modules_data = $all_node_modules_data[ count($all_node_modules_data) - 1 ];
                //
                // if ( !in_array( $node_modules_data['info']['http_code'], [404, 301, 302, 307] ) ) {
                //     if ( $node_modules_data['info']['http_code'] != 200 && !preg_match( '/<meta/', $node_modules_data['body'] ) ) {
                //         $issue_found = true;
                //         $result->add_message( 'There is a node_modules folder on the live site. This should be deleted.', 'warning' );
                //     }
                // }
            }



            // Check for Contact Form 7 reCaptcha v3 issue.

            if ( $launched && $domain_valid ) {
                $ssh = $auditor->get_ssh_connection();

                $command_result = $ssh->send_command( 'wp plugin get contact-form-7 --field=version --quiet --skip-plugins --skip-themes' );
                if ( !empty( $command_result ) ) {
                    $plugin_version = $command_result[0];

                    if ( preg_match( '/^([0-9]+)(\.[0-9]+)+/', $plugin_version ) ) {
                        if ( version_compare( $plugin_version, '5.1' ) >= 0 ) {
                            // CF7 plugin is installed and at least version 5.1.
                            // Check for incorrect reCaptcha keys.
                            $command_result = $ssh->send_command( 'wp option get wpcf7 --format=json --quiet --skip-plugins --skip-themes' );
                            if ( !empty( $command_result ) ) {
                                $wpcf7_option = json_decode( $command_result[0], true );

                                if (
                                    (isset($wpcf7_option['recaptcha_v2_v3_warning']) && $wpcf7_option['recaptcha_v2_v3_warning']) ||
                                    (version_compare( $wpcf7_option['version'], '5.1' ) < 0 && isset($wpcf7_option['recaptcha']) && !empty($wpcf7_option['recaptcha'])) ) {
                                        // WPCF7 has already detected a v2->v3 issue, or the options haven't been updated for the new version yet and it will detect a problem eventually.
                                        $issue_found = true;
                                        $result->add_message( 'Contact Form 7 has been updated to version 5.1 and requires new reCaptcha v3 keys.', 'warning' );
                                }
                            }
                        }
                    }
                }
            }









            // Check for installed plugins.
            if ( $launched && $domain_valid ) {
                $ssh = $auditor->get_ssh_connection();

                $command_result = $ssh->send_command( 'wp plugin list --status=active --format=csv --quiet --skip-themes --skip-plugins' );
                $auditor->get_logger()->log_var( $command_result );

                $active_plugins = [];

                if ( is_array( $command_result ) ) {
                    for ( $i = 1; $i < count( $command_result ); $i++ ) {
                        if ( substr_count( $command_result[$i], ',' ) == 3 ) {
                            $pieces = explode( ',', $command_result[$i] );
                            $active_plugins[] = [
                                'name' => $pieces[0],
                                'version' => trim( $pieces[3] )
                            ];
                        }
                    }
                }

                $auditor->add_meta_data( 'active_plugins', $active_plugins );
            }






            // Check for atemplatesbp account.
            if ( $launched && $domain_valid ) {
                $ssh = $auditor->get_ssh_connection();

                $command_result = $ssh->send_command( 'wp user get atemplatesbp --field=login --quiet --skip-plugins --skip-themes' );
                if ( $command_result ) {
                    if ( strpos( $command_result[0], 'Invalid user ID' ) === false ) {
                        $issue_found = true;
                        $result->add_message( 'The atemplatesbp account has not been deleted from this site.', 'warning' );
                    }
                }
            }





            /////

            if ( !$issue_found ) {
                $result->add_message( 'No miscellaneous issues found.', 'passed' );
            }
        } else {
            $auditor->get_logger()->log( 'Skipping misc tests because site is not enabled.' );
        }

        return $result;
    }
}



/*
    a:3:{
        s:7:"version";s:3:"4.9";
        s:13:"bulk_validate";a:4:{
            s:9:"timestamp";d:1551459346;
            s:7:"version";s:3:"4.9";
            s:11:"count_valid";i:1;
            s:13:"count_invalid";i:0;
        }
        s:9:"recaptcha";a:1:{
            s:40:"6LdB75QUAAAAAH0qNT6eOwwglrFXjQgq9Sp8jxTN";
            s:40:"6LdB75QUAAAAADXAk5oCGhNQCxBXrtA2GY3_qZbw";
        }
    }

    a:4:{
        s:7:"version";s:5:"5.1.1";
        s:13:"bulk_validate";a:4:{
            s:9:"timestamp";d:1551459346;
            s:7:"version";s:3:"4.9";
            s:11:"count_valid";i:1;
            s:13:"count_invalid";i:0;
        }
        s:9:"recaptcha";N;
        s:23:"recaptcha_v2_v3_warning";b:1;
    }

    a:4:{
        s:7:"version";s:5:"5.1.1";
        s:13:"bulk_validate";a:4:{
            s:9:"timestamp";d:1551459346;
            s:7:"version";s:3:"4.9";
            s:11:"count_valid";i:1;
            s:13:"count_invalid";i:0;
        }
        s:9:"recaptcha";a:1:{
            s:40:"6LdjXpUUAAAAAKxk4WVfyIMt_4pV_odCUwoZlGp9";
            s:40:"6LdjXpUUAAAAAO-iWuaKum7HWNDheBAZefW6mmzY";
        }
        s:23:"recaptcha_v2_v3_warning";b:0;
    }
*/

// wp plugin get contact-form-7 --field=version
// wp option get wpcf7 --format=json
