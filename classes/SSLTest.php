<?php

class SSLTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('ssl');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        $enabled = ( $auditor->get_test_result('enabled')->get_status() == 'passed' );
        $launched = ( $auditor->get_test_result('launched')->get_status() == 'passed' );

        if ( $enabled && $launched ) {
            $site_data = $auditor->get_site_data();
            $num_site_data = count($site_data);

            $last_site_data = $site_data[ $num_site_data - 1 ];

            if ( $last_site_data['info']['primary_port'] == 443 ) {
                $result->add_message('Site is being served over HTTPS.', 'passed');

                // Make an SSL connection to the site and verify the certificate info.
                $url = $last_site_data['url'];
                $url_matches = [];
                if ( preg_match( '#^https?://([^/]+)#', $url, $url_matches ) ) {
                    $domain = $url_matches[1];
                    $stream_context = stream_context_create([
                        'ssl' => [
                            // Don't fail to create the connection if peer verification fails, otherwise
                            // we can't check if it's correct or not.
                            'verify_peer' => false,

                            'capture_peer_cert' => true
                        ]
                    ]);

                    $stream_client_error_num = 0;
                    $stream_client_error_string = '';

                    $stream_client = @stream_socket_client(
                        'ssl://' . $domain . ':443',
                        $stream_client_error_num,
                        $stream_client_error_string,
                        30,
                        STREAM_CLIENT_CONNECT,
                        $stream_context
                    );

                    if ( $stream_client ) {
                        $stream_client_info = stream_context_get_params( $stream_client );
                        stream_socket_shutdown( $stream_client, STREAM_SHUT_RDWR );

                        $certificate_resource = $stream_client_info['options']['ssl']['peer_certificate'];
                        if ( $certificate_resource ) {
                            $certificate_info = openssl_x509_parse( $certificate_resource );
                            $subject = $certificate_info['subject']['CN'];
                            $expire_time = $certificate_info['validTo_time_t'];
                            $certificate_issuer = $certificate_info['issuer']['O'];

                            $valid_certificate = true;

                            $domain_test_result = $auditor->get_test_result( 'domain' );
                            $is_using_cloudflare_caching = $domain_test_result->has_flag( 'cloudflare_caching' );

                            if ( $subject != $domain && 'www.' . $subject != $domain && $subject != '*.' . $domain ) {
                                if ( $is_using_cloudflare_caching && strpos( $subject, '.cloudflaressl.com' ) ) {
                                    $result->add_message( "Site is using Cloudflare's SSL certificate (issued by " . $certificate_issuer . ") because the site is using Cloudflare caching.", 'info' );
                                } else {
                                    $result->add_message( 'Site is using an SSL certificate (issued by ' . $certificate_issuer . ') for ' . $subject . ' instead of ' . $domain . '.', 'error' );
                                    $valid_certificate = false;
                                }
                            }

                            if ( $valid_certificate ) {
                                if ( $expire_time <= time() ) {
                                    $result->add_message( 'Site is using an expired SSL certificate (issued by ' . $certificate_issuer . ').', 'error' );
                                } else {
                                    $result->add_message( 'Site is using a valid SSL certificate (issued by ' . $certificate_issuer . ').', 'passed' );
                                }
                            }
                        }
                    }
                }
            } else {
                $result->add_message('Site is NOT being served over HTTPS.', 'info');
            }
        } else {
            $auditor->get_logger()->log( 'Skipping SSL test because site is either not enabled or not launched.' );
        }

        return $result;
    }
}
