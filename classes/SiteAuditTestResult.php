<?php

/**
 * Holds information for a single auditor test.
 */
class SiteAuditTestResult {
    /**
     * @var array Priority of test statuses, lower number is more important.
     */
    private static $status_priorities = [
        'passed' => 20,
        'info' => 10,
        'warning' => 5,
        'error' => 1
    ];

    /**
     * @var array
     */
    private $messages;

    /**
     * @var array
     */
    private $flags;

    /**
     *
     */
    public function __construct() {
        $this->messages = [];
        $this->flags = [];
    }

    /**
     * Retrieve the priority value for a status.
     * @param string $status
     */
    private function get_status_priority($status) {
        return array_key_exists( $status, self::$status_priorities ) ? self::$status_priorities[ $status ] : 1000;
    }

    /**
     * Sorting callback to sort messages by status priority.
     * @param array $m1
     * @param array $m2
     */
    public function sort_messages_callback($m1, $m2) {
        return ( $this->get_status_priority($m1['status']) - $this->get_status_priority($m2['status']) );
    }

    /**
     * Set a boolean flag on this test result.
     * @param string $flag_name
     * @param bool $value
     */
    public function set_flag( $flag_name, $value = true ) {
        $this->flags[ $flag_name ] = !!$value;
    }

    /**
     * Determine if the specified flag has been set.
     * @param string $flag_name
     * @return bool
     */
    public function has_flag( $flag_name ) {
        return (array_key_exists( $flag_name, $this->flags) && $this->flags[ $flag_name ]);
    }

    /**
     * Add a message to the test result.
     * @param string $message
     * @param string $status passed|info|warning|error
     */
    public function add_message($message, $status = 'passed') {
        $this->messages[] = [
            'message' => $message,
            'status' => $status
        ];

        // Sort messages by priority.
        usort($this->messages, array($this, 'sort_messages_callback'));
    }

    /**
     * Retrieve the highest priority status of all messages in this test result.
     * @return string
     */
    public function get_status() {
        return empty( $this->messages ) ? 'passed' : $this->messages[ 0 ]['status'];
    }

    /**
     * Retrieve the number of messages in this result.
     * @return int
     */
    public function get_num_messages() {
        return count( $this->messages );
    }

    /**
     * @return array
     */
    public function to_array() {
        return [
            'status' => $this->get_status(),
            'messages' => $this->messages
        ];
    }
}
