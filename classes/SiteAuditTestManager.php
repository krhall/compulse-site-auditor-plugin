<?php

/**
 * Holds auditor test information.
 */
final class SiteAuditTestManager {
    private static $instance = null;

    public static function get_instance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private $tests;

    private function __construct() {
        $this->tests = [];
    }

    public function add_test(SiteAuditTest $test) {
        $this->tests[ $test->get_name() ] = $test;
    }

    public function get_all_tests() {
        return array_values( $this->tests );
    }

    public function get_all_test_names() {
        return array_keys( $this->tests );
    }

    public function get_test($name) {
        return array_key_exists($name, $this->tests) ? $this->tests[ $name ] : null;
    }
}
