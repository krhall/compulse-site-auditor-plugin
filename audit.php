<?php

/**
 * This file sets up auditor tests with the SiteAuditTestManager.
 * This ensures the auditor can run on its own, without needing to be in a plugin.
 */


define("COMPULSE_AUDIT_DIR", dirname(__FILE__));

function _audit_autoload($class) {
    $file = COMPULSE_AUDIT_DIR . '/classes/' . $class . '.php';

    if ( is_readable($file) ) {
        include $file;
    }
}
spl_autoload_register('_audit_autoload');



// Add tests.
$manager = SiteAuditTestManager::get_instance();

$manager->add_test(new EnabledTest());
$manager->add_test(new LaunchedTest(), ['enabled']);
$manager->add_test(new DomainTest(), ['launched']);
$manager->add_test(new SearchEngineVisibilityTest(), ['domain']);
$manager->add_test(new GoogleAnalyticsTest(), ['domain']);
$manager->add_test(new CoreVersionTest(), ['domain']);
$manager->add_test(new SSLTest(), ['launched']);
$manager->add_test(new MiscTest(), ['domain']);
$manager->add_test(new DiskUsageTest());
